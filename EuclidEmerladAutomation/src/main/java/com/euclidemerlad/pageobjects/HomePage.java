/**
 * 
 */
package com.euclidemerlad.pageobjects;

import com.euclidemerlad.base.BaseClass;

/**
 * @author rajivjha
 *
 */
public class HomePage extends BaseClass {
	
	public String getCurrUrl() {
		String homePageURL = getDriver().getCurrentUrl();
		return homePageURL;
	}

}
