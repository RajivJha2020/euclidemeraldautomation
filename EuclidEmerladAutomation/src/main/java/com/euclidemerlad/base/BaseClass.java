package com.euclidemerlad.base;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;
import com.euclidemerlad.actiondriver.Action;
import com.euclidemerlad.dataprovider.DataProviders;
import com.euclidemerlad.utility.ExtentManager;

/**
 * @author rajivjha
 *
 */ 

public class BaseClass {
	public static Properties prop;
	//public static WebDriver driver;
	
	// Declare ThreadLocal Driver
	public static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();

	public static WebDriver getDriver() {
		// Get Driver from threadLocalmap
		return driver.get();
	}
	
	@BeforeSuite(groups = {"Smoke","Sainity"})
	public void loadConfig() {
		ExtentManager.setExtent();
		DOMConfigurator.configure("log4j.xml");
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "\\Configuration\\Config.properties");
			prop.load(ip);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
		
	public void launchApp(String browserName) {
		 WebDriverManager.chromedriver().setup();
		if (browserName.contains("Chrome")) {
            // driver = new ChromeDriver();
			// Set Browser to thread local map.
			driver.set(new ChromeDriver());
		} else if (browserName.contains("Firefox")) {
			// driver = new FirefoxDriver();
				driver.set(new FirefoxDriver());
		} else if (browserName.contains("IE")) {
           //driver = new InternetExplorerDriver();
			driver.set(new InternetExplorerDriver());

		}
		//Action.implicitWait(driver, 10);
		//Action.pageLoadTimeOut(driver, 20);
		getDriver().manage().window().maximize();
		getDriver().get(prop.getProperty("url"));

	}
	
	@AfterSuite(groups = {"Smoke","Sainity"})
	public void afterSuit() {
		ExtentManager.endReport();
	}
}
